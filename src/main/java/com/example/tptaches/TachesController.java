package com.example.tptaches;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/taches")
public class TachesController {

  private final List<Tache> tacheList = new ArrayList<>();

  @GetMapping
  public String getTaches(Model model) {
    model.addAttribute("taches", tacheList);
    return "liste-taches";
  }

  @GetMapping("/ajout")
  public String getFormulaire(Model model) {
    model.addAttribute("tache", new Tache());
    return "formulaire-tache";
  }

  @PostMapping
  public String creationTache(@ModelAttribute("tache") Tache tache) {
    tacheList.add(tache);
    return "redirect:/taches";
  }
}
