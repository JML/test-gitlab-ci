package com.example.tptaches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpTachesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpTachesApplication.class, args);
	}

}
